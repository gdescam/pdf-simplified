# PDF Simplified

This script provides a simpler and reduced command line interface to manipulate PDF files using Ghostscript, ImageMagick and poppler in the backend.

## Deployment

1. **Clone or download the project**  
   Fork the repository or download the project files to your local machine.

2. **Update your PATH**  
   - Edit `~/.bash_profile` (for bash users) or `~/.zshrc` (for zsh users on MacOS) and add the following line:  
     ```bash
     export PATH=$PATH:/path/to/your/scripts
     ```  
   - Save the file, then apply the changes by running:  
     ```bash
     source ~/.bash_profile   # for bash  
     source ~/.zshrc          # for zsh on MacOS
     ```  
   - Verify the change by running:  
     ```bash
     echo $PATH
     ```  
   Ensure the path to your script is correctly included.

3. **Configure the deployment**  
   - Edit the `deploy.config` file and set `SCRIPTS_DIR` to:  
     ```bash
     SCRIPTS_DIR="/path/to/your/scripts"
     ```

4. **Ensure the deploy script is executable**  
   Make the deployment script executable by running:  
   ```bash
   chmod +x deploy.sh
   ```

5. **Run the deployment script**  
   Execute the deployment script:  
   ```bash
   ./deploy.sh
   ```

6. **Configure your script**  
   Edit `pdfs.config` in your script directory by following the instructions included, or leave the values by default


### Windows (Cygwin)

7. **Install Ghostscript**  
   Install the latest version of Ghostscript via the Cygwin setup.

8. **Install ImageMagick for image conversion**  
   If image conversion is required, install the latest version of ImageMagick. If unavailable, update `pdfs.config` accordingly.

9. **Install AbiWord for Word file conversion**  
   Install AbiWord and the OpenWriter plugin for converting Word documents.

10. **Install Poppler for extracting images**  
    Install the latest version of Poppler to extract images from PDFs.

11. **You're all set!**  
    Enjoy using the scripts!

### MacOS (Terminal)

7. **Install Ghostscript**  
   Install the latest version of Ghostscript via Homebrew:  
   ```bash
   brew install ghostscript
   ```

8. **Install ImageMagick for image conversion**  
   If image conversion is required, install the latest version of ImageMagick:  
   ```bash
   brew install imagemagick
   ```  
   If unavailable, update `pdfs.config` accordingly.

9. **Install Poppler for extracting images**  
   Install the latest version of Poppler via Homebrew:  
   ```bash
   brew install poppler
   ```

10. **You're all set!**  
    Enjoy using the scripts!


## Usage

```
pdfs [option...] [input_files]

    -c, compress each input PDF file to a new PDF file
        -c is followed by a space and a compression value from 1 to 5
        or it is followed by a space and \"all\" to generate 5 compressions with each value
        If no compression value is provided, the default value 4 shall be used
        The new PDF file is suffixed by -c and the compression value
    -m, merge all input PDF, image or Word files to a new PDF file called merged.pdf
        Images can be of format JPEG or PNG
        Word files can be of format DOCX or DOC
        Can be followed by the -c option for compressing the merged file
    -i, convert all input JPEG and PNG image files to new PDF files with the same name and the PDF extension
    -e, extract all images from each input PDF file into a directory named after the given PDF file
    -s, split each input PDF file into a series of PDF files
        Each produced PDF file is made of a fixed number of pages called chunk size
        -s is followed by the chunck size, 1 being the default value

Only available on Cygwin:

    -d, convert all input Word doc or docx documents to new PDF files with the same name
    -k, keep pages from a PDF file when making a new one
        Should be followed by the pages that you want to keep, separated by a comma
        Ranges are indicated using a dash; omitting a stop in the range keeps the pages until the end
        Use L to indicate the last page of the file
        The new PDF file is suffixed by -k
   
    -h, show help - usage menu
    -v, show version of the script
    -V, show versions of the script and its dependencies
    -r, rename PDF files in a specified directory
```

## Examples

```
# compress with default compression value (4)
pdfs -c doc.pdf
# compress with compression value 1
pdfs -c 1 doc.pdf
# compress with all compression values
pdfs -c all doc.pdf
# convert all corresponding images in PDF files
pdfs -i img.jpg img.png
# convert the word file in one PDF file
pdfs -d doc.docx
# merge the two files in one file
pdfs -m doc1.pdf doc2.pdf
# merge all corresponding files in one PDF file
pdfs -m *.pdf *.jpg *.docx
# merge then compress several files
pdfs -mc doc*.pdf
# split in several PDF files of one page
pdfs -s doc.pdf
# split in several PDF files of three pages
pdfs -s 3 doc.pdf
# extract all images from the file
pdfs -e doc.pdf
# keep the first, the fifth and the last page of a PDF file
pdfs -k 1,5,L doc.pdf
# keep the pages from 10 to 20, then 30 to the end of a PDF file
pdfs -k 10-20,30- doc.pdf
# rename PDF files in test/ with new name doc-1.pdf and incrementing
pdfs -r test doc
```

## Input and Output

| action | output type | name | output location |
| ------ | ------ | ------ | ------ |
| -c, compress | file | input-c | close to the input file |
| -m, merge | file | merged.pdf | close to the script |
| -i, convert images | file | input.pdf | close to the input file |
| -e, extract images | directory/files | input/image | close to the input file |
| -d, convert word | file | input.pdf | close to the input file |
| -s, split | directory/files | input/page | close to the input file |
| -k, keep | file | input-k | close to the input file |

## Errors

Most errors are hidden from the user and logged in the "pdfs_errors.log" file, located in the same directory as the script.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)

