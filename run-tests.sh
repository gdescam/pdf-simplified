#!/usr/bin/env bash
#-----------------------------------------------------------
# Project : PDF simplified
# Script  : run-tests.sh
# Author  : Gregory Descamps
# Date    : 11/08/2024
# Depend. : -
#-----------------------------------------------------------

TEST_SCRIPTS=("./pdfs-test" "./pdfs-itest")

export DEBUG_MODE=0

for TEST_SCRIPT in "${TEST_SCRIPTS[@]}"; do
	"$TEST_SCRIPT" > /dev/null 2>&1
	STATUS=$?

	if [ $STATUS -ne 0 ]; then
		echo "$TEST_SCRIPT failed"
		exit 1
	else
		echo "$TEST_SCRIPT passed"
	fi
done

echo "All tests passed successfully."