#!/usr/bin/env bash
#-----------------------------------------------------------
# Project : PDF simplified
# Script  : deploy.sh
# Author  : Gregory Descamps
# Date    : 11/08/2024
# Depend. : -
#-----------------------------------------------------------

SCRIPTS=("pdfs")
ADDITIONAL_FILES=("pdfs.config")

# Load the config file
CONFIG_FILE="deploy.config"
if [ -f "$CONFIG_FILE" ]; then 
    source "$CONFIG_FILE"
else
    echo "Error: Configuration file '$CONFIG_FILE' not found."
    exit 1
fi

if [ -z "$SCRIPTS_DIR" ]; then
    echo "Error: SCRIPTS_DIR is not defined in the configuration file."
    exit 1
elif [ ! -d "$SCRIPTS_DIR" ]; then
    echo "Directory '$SCRIPTS_DIR' does not exist. Creating it now..."
    mkdir -p "$SCRIPTS_DIR"
    if [ $? -ne 0 ]; then
        echo "Error: Failed to create directory '$SCRIPTS_DIR'."
        exit 1
    else
        echo "Directory '$SCRIPTS_DIR' created successfully."
    fi
fi

# Copy the product files
FILES=("${SCRIPTS[@]}" "${ADDITIONAL_FILES[@]}")
for file in "${FILES[@]}"; do
    if [ -f "$file" ]; then
		cp "$file" "$SCRIPTS_DIR/" || { echo "Error: Failed to copy '$file' into $SCRIPTS_DIR."; exit 1; }
    else
        echo "Error: File '$file' not found."
        exit 1
    fi
done

# Make the script files executable
for script in "${SCRIPTS[@]}"; do
    chmod +x "$SCRIPTS_DIR/$script"
    if [ $? -ne 0 ]; then
        echo "Error: Failed to make '$script' executable."
        exit 1
    fi
done

# Verify the presence of additional files in the target directory
for file in "${ADDITIONAL_FILES[@]}"; do
    if [ ! -f "$SCRIPTS_DIR/$file" ]; then
        echo "Error: '$file' not found in '$SCRIPTS_DIR' after installation."
        exit 1
    fi
done

# Verify the installed scripts
for script in "${SCRIPTS[@]}"; do
    if [ -x "$SCRIPTS_DIR/$script" ]; then
        VERSION=$("$SCRIPTS_DIR/$script" -v 2>/dev/null)
        if [ $? -eq 0 ]; then
            echo "$VERSION has been successfully installed in $SCRIPTS_DIR"
        else
            echo "Error: Failed to retrieve the version of '$script'."
            exit 1
        fi
    else
        echo "Error: Script '$script' is not executable."
        exit 1
    fi
done
